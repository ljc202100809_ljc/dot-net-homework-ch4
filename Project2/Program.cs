﻿using System;

namespace Project2
{
    class Student
    {
        public string sno { get; set; }
        public string name { get; set; }
        public int age { get; set; }
        public string sex { get; set; }
        public Student(string sno, string name, int age, string sex)
        {
            this.sno = sno;
            this.name = name;
            this.age = age;
            this.sex = sex;
        }

    }
    class Class
    {
        Student[] students;
        public Class(int capacity)
        {
            students = new Student[capacity];
        }
        public Student this[int index]
        {
            get 
            {
                if (index < 0 || index >= students.Length)
                {
                    Console.WriteLine("索引无效");
                }
                return students[index];

            }
            set
            {
                if (index < 0 || index >= students.Length)
                {
                    Console.WriteLine("索引无效");
                    return;

                }
                students[index] = value;
            }

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Class stu = new Class(3);
            Student first = new Student("202100809", "ljc", 21, "M");
            Student second = new Student("202100810", "ghf", 22, "M");
            Student third = new Student("202100800", "xxx", 19, "W");
            stu[0] = first;
            stu[1] = second;
            stu[2] = third;
            Student s1 = stu[0];
            Console.WriteLine("学号{0}\t姓名{1}\t年龄{2}\t性别{3}",s1.sno,s1.name, s1.age, s1.sex);
            Student s2 = stu[1];
            Console.WriteLine("学号{0}\t姓名{1}\t年龄{2}\t性别{3}", s2.sno, s2.name, s2.age, s2.sex);
            Student s3 = stu[2];
            Console.WriteLine("学号{0}\t姓名{1}\t年龄{2}\t性别{3}", s3.sno, s3.name, s3.age, s3.sex);

        }
    }
}
