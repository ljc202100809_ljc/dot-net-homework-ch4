﻿using System;

namespace Project1
{
    class MyStack<T>
    {
        public int Stackpointer = 0;
        public T[] Items;
        public int MaxStack;
  
        public MyStack(int i)
        {
            MaxStack = i;
        }
        public void InitStack()
        {
            Items = new T[MaxStack];
        }
        public bool IsStackfull()
        {
            if (Stackpointer >= MaxStack) return true;
            else return false;
        }
        public bool IsStackempty()
        {
            if (Stackpointer <= 0) return true;
            else return false;
        }
        public void ClearStack()
        {
            if (!IsStackempty())
            {
                Pop();
            }
        }
        public void Push(T item)
        {
            if (!IsStackfull())
            {
                Items[Stackpointer++] = item;
            }
        }
        public T Pop()
        {
            if (!IsStackempty())
            {
                return Items[--Stackpointer];
            }
            else
            {
                return Items[0];
            }
           
        }
        public void Print()
        {
            for (int i = Stackpointer - 1; i >= 0; i--)
            {
                Console.WriteLine("Value: {0}", Items[i]);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var stackInt = new MyStack<int>(10);
            var stackString = new MyStack<string>(10);
            stackInt.Push(1);
            stackInt.Push(0);
            stackString.Push("ljc");
            stackString.Push("zcr");
            stackString.Push("lxl");
            stackString.Print();
        }
    }
}
