﻿using System;

namespace Project3
{
   

    public delegate void GradePrint(Student s);
    public class Student
    {

        public string Gno { get; set; }
        public string Name { get; set; }
        public double Chinese { get; set; }
        public double Maths { get; set; }

        public double English { get; set; }
        public GradePrint GP { get; set; }
        public Student(string G, string N, double C, double M, double E)
        {
            Gno = G;
            Name = N;
            Chinese = C;
            Maths = M;
            English = E;
        }        
    }
    public class GradeReport
    {

        public static void  GradeReportOrderByTerm(Student s)
        {
            Console.WriteLine("学号{0}\t姓名{1}\t语文成绩{2}\t数学成绩{3}\t英语成绩{4}", s.Gno, s.Name, s.Chinese, s.Maths, s.English);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student s1 = new Student("1001","ljc",105,91,119);
            GradePrint g1 = GradeReport.GradeReportOrderByTerm;
            GradeReport.GradeReportOrderByTerm(s1);
        }
    }
}
